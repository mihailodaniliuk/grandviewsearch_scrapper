import time

import requests
from bs4 import BeautifulSoup
import lxml
import re
import csv
import aiohttp
import asyncio


async def get_page(session, url):
    while True:
        try:
            async with session.get(url) as r:
                if r.status == 200:
                    return await r.text()
        except Exception as e:
            print(e)


async def get_all(session, urls):
    tasks = []
    for url in urls:
        task = asyncio.create_task(get_page(session, url))
        tasks.append(task)
    results = await asyncio.gather(*tasks)
    return results


async def get_all_data_urls(urls, limit=5):
    timeout = aiohttp.ClientTimeout(total=600)
    connector = aiohttp.TCPConnector(limit=limit)
    async with aiohttp.ClientSession(connector=connector, timeout=timeout) as session:
        data = await get_all(session, urls)
        return data


def get_200_response(url, params=None, headers=None, cookies=None):
    while True:
        try:
            response = requests.get(url, params=params, headers=headers, cookies=cookies)
            if response.status_code == 200:
                return response
            else:
                print(f'Status code: {response.status_code}')
        except Exception as e:
            print(e)


def get_all_category_links():
    sub_categories = []
    response = get_200_response('https://www.grandviewresearch.com/')
    soup = BeautifulSoup(response.text, 'lxml')
    category_blocks = soup.find('ul', class_='mega-menu').find_all('li', class_='col-lg-3 col-sm-6')
    for category_block in category_blocks:
        sub_category_blocks = category_block.find_all('li', class_='heading_sub')
        sub_categories.extend(
            [f"https://www.grandviewresearch.com{block.find('a').get('href')}" for block in sub_category_blocks])
    return sub_categories


def get_number_of_pages(category_link):
    regex = 'Page 1 of (\d+),'
    response = get_200_response(category_link)
    soup = BeautifulSoup(response.text, 'lxml')
    pagination_block = soup.find('div', class_='pagination')
    last_page_block = pagination_block.find('p').text
    last_page = re.findall(regex, last_page_block)[0]
    print(last_page)
    return int(last_page)


def get_article_links_by_category(category_link):
    pages = get_number_of_pages(category_link)
    article_links = []
    for page in range(1, pages + 1):
        response = get_200_response(f'{category_link}/sort:Report.publish_date/direction:desc/page:{page}')
        soup = BeautifulSoup(response.text, 'lxml')
        all_articles_block = soup.find('div', class_='col-sm-9 col-md-9 col-xs-12')
        article_blocks = all_articles_block.find_all('div', class_='advanced_report_list full')
        for article_block in article_blocks:
            article_link = article_block.find('h3').find('a').get('href')
            article_links.append(f'https://www.grandviewresearch.com/{article_link}')
    print(len(article_links))
    return article_links


def get_article_data(link, html):
    soup = BeautifulSoup(html, 'lxml')
    article_content_block = soup.find('div', id='rprt_summary')
    article_title = soup.find('div', class_='report-title-r').find('h1').text.strip()
    try:
        article_content_block.find('div', class_='faq').decompose()
    except:
        pass
    article_content = article_content_block.getText().strip()
    return link, article_title, article_content


if __name__ == '__main__':
    with open('grand_view_search.csv', 'w') as file:
        writer = csv.writer(file)
        writer.writerow(('Link', 'Title', 'Content'))
        for category in get_all_category_links():
            print(category)
            article_links = get_article_links_by_category(category)
            articles_data = asyncio.run(get_all_data_urls(article_links))
            for index, article_data in enumerate(articles_data):
                data = get_article_data(article_links[index], article_data)
                writer.writerow(data)
